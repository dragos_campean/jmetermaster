# Use my custom base image
FROM dragoscampean/testrepo:jmetrubase

MAINTAINER Dragos

# Expose port for JMeter Master
EXPOSE 60000
